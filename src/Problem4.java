import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int Number;
        int sum = 0;
        int count = 0;

        do{
            System.out.print("Please input  number: ");
            Number = sc.nextInt();
            if (Number != 0) {
                sum = sum + Number;
                count++;
                System.out.println("Sum: " + sum + " Arg " + (((double)sum)/count));
            }
        } while (Number != 0);
        System.out.println("Bye");
        
        sc.close();
    }
}